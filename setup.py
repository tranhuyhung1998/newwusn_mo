from setuptools import setup

setup(
    name='newwusn_mo',
    version='1.0',
    packages=['nsgaii'],
    url='',
    license='',
    author='hung',
    author_email='',
    description='A NSGA-II Solution For WUSN\'s New Model', install_requires=['numpy', 'sortedcontainers', 'matplotlib',
                                                                              'joblib']
)
