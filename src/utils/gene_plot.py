import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

from src.algorithms.individual.gene import Gene


def extract_result(gene: Gene, x):
    max_loss = gene.f[1]

    for i in range(gene.dat.num_of_sensors):
        if gene.dat.et(i, gene.cms[i]) == max_loss:
            return i, False

    for j in range(gene.dat.num_of_relays):
        if x[j] > 0 and gene.dat.er(j, x[j]) == max_loss:
            return j, True

    return None


def plot(gene: Gene, axes: Axes3D, highlight_max=True):
    loads = gene.count_relay_load()
    cid, ctype = extract_result(gene, loads)

    # Highlight ground
    gx = np.arange(0, gene.dat.W)
    gy = np.arange(0, gene.dat.H)
    gx, gy = np.meshgrid(gx, gy)
    gz = gx * 0
    axes.plot_surface(gx, gy, gz, alpha=0.2, color='tab:brown')

    # Plot sensor assignments
    for i in range(gene.dat.num_of_sensors):
        sn = gene.dat.sensors[i]
        j = gene.cms[i]
        rn = gene.dat.relays[j]
        if highlight_max and ctype and j == cid:
            axes.plot([sn.x, rn.x], [sn.y, rn.y], [sn.z, rn.z], linewidth=1, c='tab:purple')
            continue
        else:
            axes.plot([sn.x, rn.x], [sn.y, rn.y], [sn.z, rn.z], linewidth=0.5, c='k')

    if highlight_max and not ctype:
        sn = gene.dat.sensors[cid]
        rn = gene.dat.relays[gene.cms[cid]]
        axes.plot([sn.x, rn.x], [sn.y, rn.y], [sn.z, rn.z], linewidth=1, c='tab:orange',
                  label='Max assignment (%.2f)' % gene.f[1])

    # Plot relay assignments
    BS = gene.dat.BS
    for j in range(gene.dat.num_of_relays):
        if loads[j] != 0:
            rn = gene.dat.relays[j]
            axes.plot([BS.x, rn.x], [BS.y, rn.y], [BS.z, rn.z], linewidth=0.5, c='k')

    if highlight_max and ctype:
        rn = gene.dat.relays[cid]
        axes.plot([BS.x, rn.x], [BS.y, rn.y], [BS.z, rn.z], linewidth=1, c='tab:purple',
                  label='Max assignment (%.2f)' % gene.f[1])

    # Plot base station
    axes.scatter(BS.x, BS.y, BS.z, c='g', label='Base station.')

    # Plot all unpicked relay positions
    unpicked = [gene.dat.relays[j] for j in range(gene.dat.num_of_relays) if loads[j] > 0]
    rx = list(map(lambda r: r.x, unpicked))
    ry = list(map(lambda r: r.y, unpicked))
    rz = list(map(lambda r: r.z, unpicked))
    axes.scatter(rx, ry, rz, c='c', label='Potential positions.')

    # Plot all sensors
    sx = list(map(lambda s: s.x, gene.dat.sensors))
    sy = list(map(lambda s: s.y, gene.dat.sensors))
    sz = list(map(lambda s: s.z, gene.dat.sensors))
    axes.scatter(sx, sy, sz, c='r', label='Sensors')

    # Plot all relays
    rx = list(map(lambda r: r.x, gene.dat.relays))
    ry = list(map(lambda r: r.y, gene.dat.relays))
    rz = list(map(lambda r: r.z, gene.dat.relays))
    axes.scatter(rx, ry, rz, c='b', label='Relays')


def plot_to_file(gene: Gene, path, dpi=200, highlight_max=True):
    fig = plt.figure(dpi=dpi)
    ax = Axes3D(fig)
    plot(gene, ax, highlight_max=highlight_max)
    ax.legend()
    fig.savefig(path)
