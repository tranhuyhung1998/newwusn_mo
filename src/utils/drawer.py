import matplotlib.pyplot as plt
import os


def add_line(path, label, color):
    x = []
    y = []
    l = []
    lines = open(path, 'r').readlines()
    for line in lines:
        if line == '':
            continue
        relay_cnt, e_x = line.split()
        l.append((1 / int(relay_cnt), 1 / float(e_x)))
    l.sort()
    for p in l:
        x.append(p[0])
        y.append(p[1])
    # print(x)
    # print(y)
    plt.plot(x, y, label=label, marker='o', color=color)


def init_plot(filename):
    plt.clf()
    plt.title("Pareto Fronts for " + filename.split('.')[0])
    plt.xlabel('Number of relays')
    plt.ylabel(r'$E_max$')


if __name__=='__main__':
    im_dir = '../../output/images/what'
    os.makedirs(im_dir, exist_ok=True)

    dirs = ['./output/moea_d_opt_mod', './output/moea_d_opt_mod2']
    names = ['MOEA/D mod 1', 'MOEA/D mod 2']
    colors = ['b', 'r', 'g']
    for dtype in sorted(os.listdir(dirs[0])):
        os.makedirs(os.path.join(im_dir, dtype), exist_ok=True)
        for file in sorted(os.listdir(os.path.join(dirs[0], dtype))):
            if not file.endswith('.out'):
                continue
            if all([os.path.exists(os.path.join(folder, dtype, file)) for folder in dirs]):
                init_plot(file)
                for cnt, folder in enumerate(dirs):
                    file_dir = os.path.join(folder, dtype, file)
                    # print(file_dir + ' ' + names[cnt] + ' ' + colors[cnt])
                    add_line(file_dir, names[cnt], colors[cnt])
                plt.legend()
                plt.savefig(os.path.join(im_dir, dtype, file.replace('out', 'png')))

