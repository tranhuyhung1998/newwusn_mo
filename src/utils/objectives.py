from collections import defaultdict
import pandas as pd

from src.utils.metrics import *


def avg_objectives(file: str, ind: int):
    paretos = read_pareto_multi(file)
    final_avg = 0.0
    for pareto in paretos:
        avg = 0.0
        for x in pareto:
            avg += x[ind]
        avg /= len(pareto)
        final_avg += avg
    final_avg /= len(paretos)
    return final_avg


def obj_all_tests(indir: str, algo: str, ind: int, dfs: defaultdict):
    for dtype in os.listdir(indir):
        results = {'Test': [], algo: []}
        for file in os.listdir(os.path.join(indir, dtype)):
            filedir = os.path.join(indir, dtype, file)
            results['Test'].append(file.split('.')[0])
            avg = avg_objectives(filedir, ind)
            results[algo].append(avg)
        df = pd.DataFrame(results)
        dfs[dtype].append(df.set_index('Test'))


def aggregate(algorithms: dict, out_dir: str):
    tabs = ['Relays', 'Energy']

    for i, tab in enumerate(tabs):
        dfs = defaultdict(list)
        for algo in algorithms:
            obj_all_tests(algorithms[algo], algo, i, dfs)

        writer = pd.ExcelWriter(os.path.join(out_dir, tab + '.xlsx'))
        for dtype in dfs:
            final_dfs = dfs[dtype][0]
            for other in dfs[dtype][1:]:
                final_dfs = final_dfs.join(other)
            final_dfs.to_excel(writer, dtype, index_label='Test')
        writer.save()


if __name__ == '__main__':
    algorithms = {
        'MOEA/D-LS': '../../output/text/moead_opt-mix1_detail',
        'MOEA/D': '../../output/text/moead_detail',
        'NSGA-II': '../../output/text/nsgaii_detail',
        'SPEA2': '../../output/text/spea2_detail',
        'D-MODE': '../../output/text/dmode_detail'
    }

    aggregate(algorithms, '../../output/stats')
