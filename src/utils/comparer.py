from collections import defaultdict
import pandas as pd

from src.utils.metrics import *


def calc(metric, dir_A: str, dir_B=None):
    test_dir = '../../data'
    indir_A = os.path.join(test_dir, *(dir_A.split('/')[-2:])).replace('.out', '.json')
    norm = WusnInput.from_file(indir_A).maxs

    paretos_A = read_pareto_multi(dir_A)
    if dir_B is not None:
        paretos_B = read_pareto_multi(dir_B)
        x = sum(metric(pA, pB) - metric(pB, pA) for pA in paretos_A for pB in paretos_B) \
            / (len(paretos_A) * len(paretos_B))
        return x
    else:
        x = sum(metric(pA, norm) for pA in paretos_A) / len(paretos_A)
        return x


def calc_all_tests(indir_A: str, indir_B, algo_name: str, metric, dfs: defaultdict):
    for dtype in os.listdir(indir_A):
        results = {'Test': [], algo_name: []}
        for file in os.listdir(os.path.join(indir_A, dtype)):
            filedir_A = os.path.join(indir_A, dtype, file)
            results['Test'].append(file)
            if indir_B is not None:
                filedir_B = os.path.join(indir_B, dtype, file)
                results[algo_name].append(calc(metric, filedir_B, filedir_A))
            else:
                results[algo_name].append(calc(metric, filedir_A))
        df = pd.DataFrame(results)
        dfs[dtype].append(df.set_index('Test'))


def aggregate(algorithms: dict, base, metric, out_dir: str):
    dfs = defaultdict(list)
    for algo in algorithms:
        if base == algo:
            continue
        calc_all_tests(algorithms[algo], algorithms[base] if base is not None else None, algo, metric, dfs)
    for dtype in dfs:
        final_dfs = dfs[dtype][0]
        for other in dfs[dtype][1:]:
            final_dfs = final_dfs.join(other)
        final_dfs.to_csv(os.path.join(out_dir, metric.__name__ + '_' + dtype + '.csv'))


if __name__ == '__main__':
    algorithms = {
        'MOEA/D-LS': '../../output/text/moead_opt-mix1_detail',
        'MOEA/D': '../../output/text/moead_detail',
        'NSGA-II': '../../output/text/nsgaii_detail',
        'SPEA2': '../../output/text/spea2_detail',
        'D-MODE': '../../output/text/dmode_detail'
    }
    out_dir = '../../output/tables/local_search'
    # os.makedirs(out_dir, exist_ok=True)
    # aggregate(algorithms, None, hypervolume_2_objs, out_dir)
    # aggregate(algorithms, None, delta_apostrophe_metric, out_dir)
    # aggregate(algorithms, 'MOEA/D-LS', c_metric, out_dir)
    aggregate(algorithms, None, nds, out_dir)

    # kbs_base = {
    #     'MOEA/D-LS': '../../output/text/moead_opt-mix1_detail',
    #     'KBS': '../../output/text/kbs'
    # }
    # kbs_out_dir = '../../output/tables/kbs'
    # os.makedirs(kbs_out_dir, exist_ok=True)
    # aggregate(kbs_base, None, hypervolume_2_objs, kbs_out_dir)
