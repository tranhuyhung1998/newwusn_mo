import os
import math
import numpy as np

from src.commons.input import WusnInput


def dominate(a: list, b: list):
    dominated = False
    for i in range(len(a)):
        if a[i] > b[i]:
            return False
        if a[i] < b[i]:
            dominated = True
    return dominated


def read_pareto(path: str):
    with open(path, 'r') as f:
        pareto = []
        lines = f.readlines()
        for line in lines:
            if line != '':
                res = line.split()
                pareto.append([int(res[0]), float(res[1])])
        pareto.sort()
        return pareto


def read_pareto_multi(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
        paretos = []
        i = 0
        while i < len(lines):
            if lines[i] == '':
                continue
            cnt = int(lines[i].split()[0])
            pareto = []
            for j in range(i+1, i+cnt+1):
                res = lines[j].split()
                pareto.append([int(res[0]), float(res[1])])
            i += cnt+1
            pareto.sort()
            paretos.append(pareto)
        return paretos


def euclidian_distance(v1: list, v2: list, norm: list):
    sum_dist = 0.0
    for i in range(len(norm)):
        sum_dist += abs(v1[i] - v2[i]) / norm[i]
    return math.sqrt(sum_dist)


def delta_metric(v: list, norm: list):
    size = len(v)
    if size == 1:
        return 0.0
    d_f = v[0]
    d_l = v[-1]

    distances = []
    for i in range(size - 1):
        distances.append(euclidian_distance(v[i], v[i+1], norm))
    d_avg = sum(distances) / size

    delta = euclidian_distance(d_f, d_l, norm)
    for i in range(size - 1):
        delta += abs(distances[i] - d_avg)

    delta /= (euclidian_distance(d_f, d_l, norm) + size * d_avg)
    return delta


def delta_apostrophe_metric(v: list, norm: list):
    size = len(v)
    if size == 1:
        return 0.0

    distances = []
    for i in range(size - 1):
        distances.append(euclidian_distance(v[i], v[i+1], norm))
    d_avg = sum(distances) / size

    delta = 0.0
    for i in range(size - 1):
        delta += abs(distances[i] - d_avg)

    delta /= size * d_avg
    return delta


def c_metric(v1: list, v2: list):
    cnt = 0
    for inv2 in v2:
        for inv1 in v1:
            if dominate(inv1, inv2):
                cnt += 1
                break
    return cnt / len(v2)


def nds(v: list, norm: list):
    return len(v)


def width(v: list):
    ws = []
    for i in range(2):
        objectives = [inv[i] for inv in v]
        ws.append(max(objectives) - min(objectives))
    return ws


def area(v: list):
    widths = width(v)
    return widths[0] * widths[1]


def hypervolume_2_objs(v: list, norm: list, maximize=False):
    hyp = 0.0
    if maximize:
        v.sort(key=lambda x: x[0])
        for i in range(len(v) - 1):
            hyp += v[i][0] * (v[i][1] - v[i+1][1])
        hyp += v[-1][0] * v[-1][1]
    else:
        # v.sort(key=lambda x: -x[0])
        # for i in range(len(v) - 1):
        #     hyp += (1/v[i][1] - 1/v[i+1][1]) / v[i][0]
        # hyp += 1 / v[-1][0] / v[-1][1]
        v.sort(key=lambda x: x[0])
        hyp = (norm[0] - v[0][0]) * (norm[1] - v[0][1])
        for i in range(len(v) - 1):
            hyp += (norm[0] - v[i+1][0]) * (v[i][1] - v[i+1][1])
    return hyp / norm[0] / norm[1]


def objective_value(v: list, alpha: float, norm: list):
    best = float('inf')
    for inv in v:
        ov = alpha * inv[0] / norm[0] + (1 - alpha) * inv[1] / norm[1]
        best = min(best, ov)

    return best


def quick_compare(v1: list, v2: list, norm: list):
    print("Delta:\t" + str(delta_metric(v1, norm)) + '\t' + str(delta_metric(v2, norm)))
    print("C:\t" + str(c_metric(v1, v2)) + '\t' + str(c_metric(v2, v1)))
    print("NDS:\t" + str(nds(v1)) + '\t' + str(nds(v2)))
    print("Width:\t" + str(area(v1)) + '\t' + str(area(v2)))


if __name__ == '__main__':
    in_dir = '../../data'
    cmp_dir = '../../output/tables'
    out_dir = '../../output'

    dir1 = 'moea_d'
    dir2 = 'moea_d_opt'

    os.makedirs(cmp_dir, exist_ok=True)
    with open(os.path.join(cmp_dir, dir1 + '-' + dir2 + '.csv'), 'w') as cmp_file:
        cmp_file.write('Type,Test,' + dir1 + ',,,,,' + dir2 + '\n')
        cmp_file.write(',,diversity,coverage,NDS,width_R,width_EC(e-4),'
                       'diversity,coverage,NDS,width_R,width_EC(e-4)\n')
        for dtype in sorted(os.listdir(in_dir)):
            print('---' + dtype + '---')
            cmp_res = np.zeros([2, 4], int)
            per_res = np.zeros(4, float)
            cnt = 0
            for file in sorted(os.listdir(os.path.join(in_dir, dtype))):
                file1 = os.path.join(out_dir, dir1, dtype, file.split('.')[0] + '.out')
                file2 = os.path.join(out_dir, dir2, dtype, file.split('.')[0] + '.out')
                if os.path.exists(file1) and os.path.exists(file2):
                    cnt += 1
                    norm = WusnInput.from_file(os.path.join(in_dir, dtype, file)).maxs
                    p = [read_pareto(file1), read_pareto(file2)]
                    string = dtype.split()[1].replace('-1', '0') + ',' + file.split('.')[0]

                    a = np.empty([2, 4], float)
                    for i in range(2):
                        a[i, 0] = delta_metric(p[i], norm)
                        a[i, 1] = c_metric(p[i], p[1-i])
                        a[i, 2] = nds(p[i])
                        a[i, 3] = area(p[i])
                        string = ','.join([string, format(delta_metric(p[i], norm), '.4f'),
                                           str(c_metric(p[i], p[1-i])), str(nds(p[i])),
                                           str(width(p[i])[0]), format(width(p[i])[1] * 10000, '.4f')])

                    for i in range(4):
                        if i == 1:
                            per_res[i] += a[0, i] - a[1, i]
                        elif a[1, i] != 0:
                            per_res[i] += a[0, i] / a[1, i] - 1
                        if a[0, i] > a[1, i]:
                            cmp_res[0, i] += 1
                        if a[0, i] < a[1, i]:
                            cmp_res[1, i] += 1

                    cmp_file.write(string + '\n')

            print("Delta: " + str(cmp_res[0, 0]) + ' ' + str(cmp_res[1, 0]))
            print("C: " + str(cmp_res[0, 1]) + ' ' + str(cmp_res[1, 1]))
            print("NDS: " + str(cmp_res[0, 2]) + ' ' + str(cmp_res[1, 2]))
            print("Width: " + str(cmp_res[0, 3]) + ' ' + str(cmp_res[1, 3]))

            per_res = per_res * 100 / cnt
            print("Delta: " + str(-per_res[0]) + '%')
            print("C: " + str(per_res[1]) + '%')
            print("NDS: " + str(per_res[2]) + '%')
            print("Width: " + str(per_res[3]) + '%')

    # for dtype in sorted(os.listdir(dir1)):
    #     os.makedirs(os.path.join(out_dir, dtype), exist_ok=True)
    #     with open(os.path.join(out_dir, dtype, 'comparison-single.csv'), 'w+') as out_file:
    #         out_file.write('Test,alpha,MOEA/D,Local Search\n')
    #         for file in sorted(os.listdir(os.path.join(dir1, dtype))):
    #             if os.path.exists(os.path.join(dir2, dtype, file)):
    #                 norm_path = os.path.join('./data', dtype, file.replace('out', 'norm'))
    #                 norm = []
    #                 with open(norm_path, 'r') as f:
    #                     norm_attributes = f.readlines()[0].split()
    #                     norm.append(int(norm_attributes[0]))
    #                     norm.append(float(norm_attributes[1]))
    #
    #                     p = [read_pareto(os.path.join(dir1, dtype, file)), read_pareto(os.path.join(dir2, dtype, file))]
    #
    #                     for alpha in np.arange(0.1, 0.91, 0.1):
    #                         string = file + ',' + str(alpha)
    #                         for i in range(2):
    #                             string = ','.join([string, str(objective_value(p[i], alpha, norm))])
    #                         out_file.write(string + '\n')
