import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from matplotlib.ticker import MaxNLocator


def add_line(sub_plt, points: list, label, marker, color):
    points.sort()
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    # print(x)
    # print(y)
    sub_plt.plot(x, y, label=label, marker=marker, color=color)


def draw_convergence(sub_plt, con_list: list):
    # print(con_list)
    for gen in con_list:
        add_line(sub_plt, gen['points'], label='Generation ' + str(gen['gen']),
                 marker='o', color=m.to_rgba(gen['gen']))
    sub_plt.legend()


def read_convergence(file_path):
    print(file_path)
    with open(file_path) as f:
        con_list = []
        lines = f.readlines()
        # i = 0 if lines[0].split()[1] != '0' else 1
        i = 0
        while i < len(lines):
            gen, num = lines[i].split()
            gen_points = {'gen': int(gen), 'points': []}
            for _ in range(int(num)):
                i += 1
                line = lines[i].split()
                gen_points['points'].append(tuple([int(line[0]), float(line[1])]))
            if gen_points['gen'] != 0 and gen_points['gen'] % 2 == 0 and gen_points['gen'] <= 10:
                con_list.append(gen_points)
            i += 1

        return reversed(con_list)


def init_plot(sub_plt, filename):
    sub_plt.clf()
    sub_plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
    sub_plt.title(filename.split('.')[0])
    sub_plt.xlabel('Number of relays')
    sub_plt.ylabel(r'$E_max$')


if __name__ == '__main__':
    norm = mpl.colors.Normalize(vmin=0, vmax=10)
    cmap = plt.cm.rainbow
    m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

    im_dir = '../../output/images/super'
    out_dir = '../../output/text'

    directories = ['moead_opt-mix_converge']
    for directory in directories:
        path1 = os.path.join(out_dir, directory)
        for dataset in os.listdir(path1):
            path2 = os.path.join(path1, dataset)
            os.makedirs(os.path.join(im_dir, directory, dataset), exist_ok=True)
            for file in os.listdir(path2):
                init_plot(plt, file)
                draw_convergence(plt, read_convergence(os.path.join(path2, file)))
                plt.savefig(os.path.join(im_dir, directory, dataset, file.split('.')[0] + '.png'))
