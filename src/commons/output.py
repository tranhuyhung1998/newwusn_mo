import os
import re
from collections import defaultdict

from src.utils.metrics import dominate


def find_best_of(pop: list, alpha: float):
    f_min = float('inf')
    p_min = None
    for p in pop:
        f = p.objective_function(alpha)
        if f_min > f:
            f_min = f
            p_min = p
    return p_min.f, f_min


def pop_to_pareto(pop: list):
    res = {}
    for inv in pop:
        pareto = True
        for cmp_inv in pop:
            if cmp_inv.dominate(inv):
                pareto = False
                break
        if pareto:
            res[tuple(inv.f)] = inv

    return res


def objs_to_pareto(pop: list):
    res = {}
    for inv in pop:
        pareto = True
        for cmp_inv in pop:
            if dominate(cmp_inv, inv):
                pareto = False
                break
        if pareto:
            res[tuple(inv)] = None

    return res


def pareto_con_file(pop: list, con_file, gen):
    res = pop_to_pareto(pop)
    con_file.write(str(gen) + ' ' + str(len(res)) + '\n')
    for p in res:
        con_file.write(' '.join([str(e) for e in p]) + '\n')


def pareto_to_file(res: dict, file_path, inv_path=None):
    f = open(file_path, "w")
    for p in res:
        f.write(str(p[0]) + ' ' + str(p[1]) + '\n')
    f.close()

    if inv_path is not None:
        f = open(inv_path, "w")
        for p in res.values():
            f.write(' '.join([str(e) for e in p.cms]) + '\n')
        f.close()


def detail_to_file(res: dict, time: float, file_path):
    # print(file_path)
    with open(file_path, 'a') as f:
        f.write(str(len(res)) + ' ' + str(time) + '\n')
        for p in res:
            f.write(str(p[0]) + ' ' + str(p[1]) + '\n')


def file_to_pareto(file_path):
    f = open(file_path, 'r')
    lines = f.readlines()
    s = set()
    for p in lines:
        if p != '':
            s.add(tuple([float(x) for x in p.split()]))
    return s


def file_to_pareto_multi(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
        paretos = []
        i = 0
        while i < len(lines):
            if lines[i] == '':
                continue
            cnt = int(lines[i].split()[0])
            s = set()
            for j in range(i+1, i+cnt+1):
                s.add(tuple([float(x) for x in lines[j].split()]))
            i += cnt+1
            paretos.append(s)
        return paretos


def kbs_to_dict(folder_path, cnt: int, tests: dict):
    for file in os.listdir(folder_path):
        with open(os.path.join(folder_path, file)) as f:
            res = f.readlines()[0].split()
            test = res[0]
            cid = int(re.split('[_.]', file)[3]) - 1
            relays = int(res[2])
            energy = float(res[3])
            if test not in tests:
                tests[test] = []
                for _ in range(cnt):
                    tests[test].append([])
            tests[test][cid].append([relays, energy])


def convert_kbs(output_path, dest_path):
    os.makedirs(dest_path, exist_ok=True)

    tests = dict()
    for alpha_dir in os.listdir(output_path):
        kbs_to_dict(os.path.join(output_path, alpha_dir), 10, tests)

    for test in tests:
        test_out = os.path.join(dest_path, test.replace('.in', '.out'))
        for pop in tests[test]:
            detail_to_file(objs_to_pareto(pop), 0.0, test_out)


if __name__ == '__main__':
    convert_kbs('../../output/kbs_raw/kbs/small', '../../output/text/kbs/small')
    convert_kbs('../../output/kbs_raw/kbs/medium', '../../output/text/kbs/medium')
