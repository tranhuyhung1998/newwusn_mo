import json
import numpy as np

from src.commons.point import *


class WusnConstants:
    # Unit: J
    e_elec = 50 * 1e-9
    e_fs = 10 * 1e-12
    e_mp = 0.0013 * 1e-12
    e_DA = 5 * 1e-12

    # Num of bits
    k_bit = 4000


class WusnInput:
    def __init__(self, _W=500, _H=500, _depth=1., _height=10., _num_of_relays=10, _num_of_sensors=50,
                 _radius=20., _relays=None, _sensors=None, _BS=None, static_relay_loss=None,
                 dynamic_relay_loss=None, sensor_loss=None, max_rn_conn=None):
        self.W = _W
        self.H = _H
        self.depth = _depth
        self.height = _height
        self.relays = _relays
        self.sensors = _sensors
        self.num_of_relays = _num_of_relays
        self.num_of_sensors = _num_of_sensors
        self.radius = _radius
        self.BS = _BS
        # self.get_loss()
        self.static_relay_loss = static_relay_loss
        self.dynamic_relay_loss = dynamic_relay_loss
        self.sensor_loss = sensor_loss
        self.max_rn_conn = max_rn_conn

        self.links = None
        self._e_max = None

        if None in (static_relay_loss, dynamic_relay_loss, sensor_loss):
            self.calculate_loss()
        if max_rn_conn is None:
            self.calculate_max_rn_conn()

        self.calculate_links()
        # self.mins, self.maxs = self.calculate_bounds()
        self.maxs = np.asarray((self.num_of_relays, self.e_max))

    @classmethod
    def from_file(cls, path):
        f = open(path)
        d = json.load(f)
        return cls.from_dict(d)

    @classmethod
    def from_dict(cls, d):
        W = d['W']
        H = d['H']
        depth = d['depth']
        height = d['height']
        num_of_relays = d['num_of_relays']
        num_of_sensors = d['num_of_sensors']
        radius = d['radius']
        relays = []
        sensors = []
        BS = Point.from_dict(d['center'])
        for i in range(num_of_sensors):
            sensors.append(SensorNode.from_dict(d['sensors'][i]))
        for i in range(num_of_relays):
            relays.append(RelayNode.from_dict(d['relays'][i]))

        return cls(W, H, depth, height, num_of_relays, num_of_sensors, radius, relays, sensors, BS)

    def freeze(self):
        self.sensors = tuple(self.sensors)
        self.relays = tuple(self.relays)

    def to_dict(self):
        return {
            'W': self.W, 'H': self.H,
            'depth': self.depth, 'height': self.height,
            'num_of_relays': self.num_of_relays,
            'num_of_sensors': self.num_of_sensors,
            'relays': list(map(lambda x: x.to_dict(), self.relays)),
            'sensors': list(map(lambda x: x.to_dict(), self.sensors)),
            'center': self.BS.to_dict(),
            'radius': self.radius
        }

    def to_file(self, file_path):
        d = self.to_dict()
        with open(file_path, "wt") as f:
            fstr = json.dumps(d, indent=4)
            f.write(fstr)

    # def create_cache(self):
    #     loss_file_name = str(hash(self)) + ".loss"
    #     list_loss_file = os.listdir("cache")
    #     if loss_file_name in list_loss_file:
    #         print("Cache exist")
    #     else:
    #         print("Creating cache")
    #         f = open("cache/" + loss_file_name, "wb")
    #         self.calculate_loss()
    #         data = [self.relay_loss, self.sensor_loss]
    #         pickle.dump(data, f)
    #         f.close()

    def communicable(self, sn, rn):
        return distance(sn, rn) <= 2 * self.radius

    def calculate_max_rn_conn(self):
        max_rn_conn = {}

        for rn in self.relays:
            max_rn_conn[rn] = 0
            for sn in self.sensors:
                if distance(sn, rn) <= self.radius:
                    max_rn_conn[rn] += 1
        self.max_rn_conn = max_rn_conn

    @property
    def e_max(self):
        if self._e_max is not None:
            return self._e_max

        vals = np.max(self.sensor_loss)
        max_rloss = []
        for rn in self.relays:
            max_rloss.append(WusnConstants.k_bit * (self.num_of_sensors * (WusnConstants.e_elec + WusnConstants.e_DA) +
                                                    WusnConstants.e_mp * (distance(rn, self.BS) ** 4)))
        self._e_max = max([vals, max(max_rloss)])

        return self._e_max

    def calculate_loss(self):
        sensor_loss = np.full([len(self.sensors), len(self.relays)], 0, dtype=float)
        static_relay_loss = np.empty([len(self.relays)], dtype=float)
        dynamic_relay_loss = np.empty([len(self.relays)], dtype=float)
        # R = self.radius
        BS = self.BS

        for i, sn in enumerate(self.sensors):
            for j, rn in enumerate(self.relays):
                if self.communicable(sn, rn):
                    sensor_loss[i][j] = WusnConstants.k_bit * (
                            WusnConstants.e_elec + WusnConstants.e_fs * math.pow(distance(sn, rn), 2))

        for j, rn in enumerate(self.relays):
            dynamic_relay_loss[j] = WusnConstants.k_bit * (WusnConstants.e_elec + WusnConstants.e_DA)
            static_relay_loss[j] = WusnConstants.k_bit * WusnConstants.e_mp * math.pow(distance(rn, BS), 4)

        self.static_relay_loss = static_relay_loss
        self.dynamic_relay_loss = dynamic_relay_loss
        self.sensor_loss = sensor_loss

    def calculate_links(self):
        links = np.empty([self.num_of_sensors], dtype=object)

        for i in range(len(self.sensors)):
            links[i] = []
            for j in range(len(self.relays)):
                if self.communicable(self.sensors[i], self.relays[j]):
                    links[i].append(j)
            assert len(links[i]) > 0, 'No solution!!! Exception: ' + str(self.sensors[i])

        self.links = links

    def get_min_relays(self):
        links = np.empty([self.num_of_sensors], dtype=object)
        selected_rns = np.zeros([self.num_of_relays], dtype=bool)

        for j in range(len(self.relays)):
            links[j] = list.copy(self.links[j])

        for i in range(self.num_of_sensors):
            if len(self.links[i]) == 1:
                j = self.links[i][0]
                selected_rns[j] = True
                for sn in links[j]:
                    for rn in self.links[sn]:
                        links[rn].remove(sn)

        while True:
            max_cover = 0
            max_rn = -1

            for j in range(self.num_of_relays):
                if not selected_rns[j] and max_cover < len(links[j]):
                    max_cover = len(links[j])
                    max_rn = j

            if max_cover == 0:
                break

            selected_rns[max_rn] = True
            for sn in links[max_rn]:
                for rn in self.links[sn]:
                    links[rn].remove(sn)

        return np.sum(selected_rns)

    def calculate_bounds(self):
        e_min = min(np.min(self.sensor_loss), np.min(self.dynamic_relay_loss + self.static_relay_loss))
        relays_min = self.get_min_relays()
        return np.asarray((relays_min, e_min)), np.asarray((self.num_of_relays, self.e_max))

    def max_population(self, n: int):
        s = 1
        for l in self.links:
            s = s * len(l)
            if s >= n:
                break
        return min(s, n)

    def et(self, i: int, j: int):
        return self.sensor_loss[i][j]

    def er(self, j: int, x: int):
        return self.dynamic_relay_loss[j] * x + self.static_relay_loss[j]

    def __hash__(self):
        return hash((self.W, self.H, self.depth, self.height, self.num_of_relays, self.num_of_sensors, self.radius,
                     tuple(self.relays), tuple(self.sensors)))

    def __eq__(self, other):
        return hash(self) == hash(other)


if __name__ == "__main__":
    inp = WusnInput.from_file('../data/small/uu-dem1_r25_1.in')
    print(inp.e_max)