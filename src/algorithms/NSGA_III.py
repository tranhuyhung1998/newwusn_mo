from itertools import combinations
import random
import numpy as np

from src.commons.input import WusnInput
from src.commons.output import pareto_con_file
from src.algorithms.individual.ga_gene import GAGene
from src.algorithms.module.optimizer import optimize
from src.algorithms.module.mo_selection import fast_non_dominated_sort
from src.algorithms.NSGA_II import make_new_pop


def perpendicular_distance(s, w):
    _s = np.array(s)
    _w = np.array(w)
    return np.cross(_s, _w) / np.linalg.norm(_w)


def generate_structured_reference_points(M: int, p: int):
    assert M >= 2
    assert p >= 1

    Z = []
    combs = combinations(list(range(M + p - 1)), M - 1)
    for c in combs:
        z = []
        i = 0
        for j in c:
            z.append((j - i) / p)
            i = j + 1
        z.append((M+p-1 - i) / p)
        Z.append(tuple(z))

    return Z


def normalize(M, S_t):
    f_n = {}
    for s in S_t:
        f_n[s] = s.f.copy()

    for j in range(M):
        z_min = min(s.f[j] for s in S_t)
        z_max = 0
        for s in S_t:
            f_n[s][j] -= z_min
            z_max = max(z_max, f_n[s][j])
        for s in S_t:
            f_n[s][j] /= z_max

    return f_n


def associate(S_t, Z, f_n):
    pi = {}
    d = {}

    for s in S_t:
        d[s] = float('inf')
        for w in Z:
            pd = perpendicular_distance(f_n[s], w)
            if d[s] > pd:
                pi[s] = w
                d[s] = pd

    return pi, d


def niching(K, p_j, pi, d, F_l, P):
    p_j_tmp = p_j.copy()

    k = 1
    while k <= K:
        min_p = min(p_j_tmp.values())
        J_min = [z for z, p in p_j_tmp.items() if p == min_p]
        j = random.choice(J_min)
        I_j = [s for s in F_l if pi[s] == j]
        if len(I_j) != 0:
            if p_j[j] == 0:
                s_min = I_j[0]
                for s in I_j:
                    if d[s] < d[s_min]:
                        s_min = s
            else:
                s_min = random.choice(I_j)
            P.append(s_min)
            p_j[j] += 1
            F_l.remove(s_min)
            k += 1
        else:
            del p_j_tmp[j]


def generation(M, Z, P, p_c, p_m):
    N = len(Z)
    S_t = []
    i = 0
    Q = make_new_pop(P, N, p_c, p_m)
    R = P + Q
    F = fast_non_dominated_sort(R)

    while len(S_t) < N:
        S_t.extend(F[i])
        i += 1

    i -= 1
    F_l = F[i]
    if len(S_t) == N:
        return S_t

    P = S_t[:-len(F_l)]
    K = N - len(P)
    f_n = normalize(M, S_t)
    pi, d = associate(S_t, Z, f_n)

    p_j = dict.fromkeys(Z, 0)
    for s in P:
        p_j[pi[s]] += 1

    niching(K, p_j, pi, d, F_l, P)
    return P


def NSGAIII(data: WusnInput, max_generations=250, p=None,
           p_c=0.7, p_m=0.1, opt_mode=None, con_file=None, con_step=1):
    M = 2
    if p is None:
        H = data.max_population(data.num_of_sensors)
        p = H - 1
    else:
        H = p + 1
    Z = generate_structured_reference_points(M, p)

    P = []
    while len(P) < H:
        g = GAGene(data)
        if not P.__contains__(g):
            P.append(g)

    if con_file is not None:
        pareto_con_file(fast_non_dominated_sort(P)[0], con_file, 0)

    for k in range(max_generations):
        print(k)
        P = generation(M, Z, P, p_c, p_m)
        if opt_mode is not None:
            for p in P:
                optimize(p)

        if con_file is not None and (k + 1) % con_step == 0:
            pareto_con_file(fast_non_dominated_sort(P)[0], con_file, k+1)

    P = fast_non_dominated_sort(P)
    P[0].sort(key=lambda p: p.f[0])

    # if opt_mode is not None:
    #     for p in P[0]:
    #         optimize(p)
    return P[0]


if __name__ == '__main__':
    d = WusnInput.from_file('../data/dataset 4-1/uu-dem3_r50_1.in')
    F = NSGAIII(d, 250)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))

    # print('---')
    #
    # F = NSGAIII(d, 500)
    # for p in F:
    #     print(str(p.f[0]) + ' ' + str(p.f[1]))
    #
    # print('---')
    #
    # F = NSGAIII(d, 1000)
    # for p in F:
    #     print(str(p.f[0]) + ' ' + str(p.f[1]))
