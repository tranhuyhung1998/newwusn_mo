import json
import math
import random
import sys

import numpy as np

from src.commons.output import pareto_con_file, pop_to_pareto
from src.algorithms.individual.ga_gene import GAGene
from src.commons.input import WusnInput
from src.algorithms.module.optimizer import optimize
from src.utils.metrics import quick_compare


class WusnConfig:
    def __init__(self, p_c=0.7, p_m=0.1, _max_gens=250, _max_stall_gens=50, _m=2, _N=41, _T=3, rand=0, _weight_vectors=None):
        self.p_c = p_c
        self.p_m = p_m
        self.max_gens = _max_gens
        self.max_stall_gens = _max_stall_gens

        self.m = _m

        self.N = _N
        self.T = _T
        self.weight_vectors = _weight_vectors

        if self.weight_vectors is None:
            self.weight_vectors = self.init_wvs_spread(_N, rand)

    @staticmethod
    def init_wvs_spread(N, rand):
        wvs = []
        spread = N - rand
        for i in np.arange(0, 1 + sys.float_info.epsilon, 1/(spread-1)):
            wvs.append([i, 1 - i])
        for _ in range(rand):
            alpha = np.random.random()
            wvs.append((alpha, 1 - alpha))

        return np.array(wvs)

    @classmethod
    def from_file(cls, path, N, T, gen, stall_gen):
        f = open(path)
        d = json.load(f)
        return cls.from_dict(d, N, T, gen, stall_gen)

    @classmethod
    def from_dict(cls, d, N, T, gen, stall_gen):
        p_c = d['probability']['crossover']
        p_m = d['probability']['mutation']

        if gen is None:
            gen = d['stopping_criteria']['max_generations']
        if stall_gen is None:
            stall_gen = d['stopping_criteria']['max_stall_generations']

        specific_params = d['moea_d']
        if N is None:
            N = specific_params['sub_problems']
        if T is None:
            T = specific_params['neighborhood_size']
        rand = 0
        wvs = None
        if 'weight_vectors' in specific_params:
            wvs = np.asarray(specific_params['weight_vectors'])

        return cls(p_c, p_m, gen, stall_gen, 2, N, T, rand, wvs)

    def to_dict(self):
        return {
            'probability': {
                'crossover': self.p_c,
                'mutation': self.p_m
            },
            'stopping_criteria': {
                'max_generations': self.max_gens,
                'max_stall_generations': self.max_stall_gens
            },
            'moea_d': {
                'sub_problems': self.N,
                'rand': None,
                'weight_vectors': self.weight_vectors.tolist(),
                'neighborhood_size': self.T
            }
        }

    def to_file(self, file_path):
        d = self.to_dict()
        with open(file_path, "wt") as f:
            fstr = json.dumps(d, indent=4)
            f.write(fstr)


def genitor_function(length: int, bias=1.5):
    index = (bias - math.sqrt(bias * bias - 4 * (bias - 1) * random.random())) / (2 * (bias - 1))
    return int(index * length)


class MOEA_D:

    # Step 1) Initialization
    def __init__(self, data: WusnInput, config: WusnConfig, ref_init):
        self.data = data
        self.config = config

        self.EP = []
        self.B = self.init_neighborhood()
        self.x = self.init_population()
        if ref_init is not None:
            self.z = self.init_reference_point()
        else:
            self.z = GAGene(self.data, False)
            self.z.f = []
            self.z.f.append(min([x.f[0] for x in self.x]))
            self.z.f.append(min([x.f[1] for x in self.x]))

    # 1.2)
    def init_neighborhood(self):
        B = np.empty([self.config.N, self.config.T], dtype=int)
        for i in range(self.config.N):
            wv = self.config.weight_vectors[i]
            euclidean_distances = np.empty([self.config.N], dtype=float)
            for j in range(self.config.N):
                euclidean_distances[j] = np.linalg.norm(wv - self.config.weight_vectors[j])
            B[i] = np.argsort(euclidean_distances)[:self.config.T]
        # print(B)
        return B

    # 1.3)
    def init_population(self):
        pop = np.empty([self.config.N], dtype=GAGene)
        for i in range(self.config.N):
            pop[i] = GAGene(self.data)
        # pop = np.fromfunction(lambda self, x: Gene(self.data), (self.config.N, self.config.S), dtype=Gene)
        # for i in range(self.config.N):
        #     order = np.argsort(np.vectorize(self.g_te)(pop[i], self.config.weight_vectors[i]))
        #     pop[i] = (pop[i])[order]
        return pop

    # 1.4)
    def init_reference_point(self):
        greedy_gene = GAGene(self.data, False)
        for i in range(self.data.num_of_sensors):
            sn_links = self.data.links[i]
            greedy_gene.cms[i] = sn_links[0]
            for j in sn_links:
                if self.data.sensor_loss[i][j] < self.data.sensor_loss[i][greedy_gene.cms[i]]:
                    greedy_gene.cms[i] = j
        greedy_gene.update()
        return greedy_gene

    # Step 2) Update + 3) Stopping Criteria
    def g_te(self, x: GAGene, weight_vector):
        return np.max(weight_vector * np.abs(x.norm() - self.z.norm()))

    def solve(self, opt_step=50, opt_mode='mix', con_file=None, con_step=50, per=1.0, cms=True):
        if con_file is not None:
            pareto_con_file(self.EP, con_file, 0)
        no_improve_seq = 0

        for gen in range(self.config.max_gens):
            improved = False
            for i in range(self.config.N):
                # 2.1)
                kk = np.random.randint(0, len(self.B[i]))
                ll = np.random.randint(0, len(self.B[i]) - 1)
                if ll >= kk:
                    ll = ll + 1

                y = GAGene.relay_crossover(self.x[self.B[i][kk]], self.x[self.B[i][ll]])[np.random.randint(2)]
                y.mutation(self.config.p_m)

                # 2.2)
                if opt_step > 0 and (gen + 1) % opt_step == 0 and random.random() < per:
                    optimize(y, opt_mode if opt_mode != 'mix' else self.config.weight_vectors[i][0])
                # if not y.validate():
                #     print("Bugged!!!")
                #     exit(-1)

                # 2.3)
                for j in range(self.config.m):
                    if self.z.f[j] > y.f[j]:
                        self.z.f[j] = y.f[j]

                # 2.4)
                for j in self.B[i]:
                    wv = self.config.weight_vectors[i]
                    if self.g_te(y, wv) < self.g_te(self.x[j], wv):
                        self.x[j] = y

                # 2.5)
                self.EP = [sol for sol in self.EP if not y.dominate(sol)]
                y_dominated = False
                for sol in self.EP:
                    if sol.dominate(y) or y.f == sol.f:
                        y_dominated = True
                        break
                if not y_dominated:
                    self.EP.append(y)
                    improved = True

            if improved:
                no_improve_seq = 0
            else:
                no_improve_seq += 1

            if no_improve_seq == self.config.max_stall_gens:
                if con_file is not None:
                    pareto_con_file(self.EP, con_file, gen+1)
                break
            if con_file is not None and (gen + 1) % con_step == 0:
                pareto_con_file(self.EP, con_file, gen+1)

        return self.EP


if __name__ == '__main__':
    config = WusnConfig.from_file('../../config.json', 41, 3, 10, 10)
    data = WusnInput.from_file('../../data/medium/uu-dem4_r45_1.json')
    v1 = MOEA_D(data, config, None).solve(opt_step=1)
    with open('../../temp.txt', 'w') as file:
        for p in v1:
            file.write(' '.join([str(x) for x in list(p.cms)]) + '\n')
    # v2 = MOEA_D(data, config, None).solve(opt_step=1, per=0.5)
    # quick_compare([x.f for x in v1], [x.f for x in v2], data.maxs)

    # with open('temp.txt', 'w') as file:
    #     res = pop_to_pareto(MOEA_D(data, config, None).solve(opt_step=1))
    #     config.max_gens = 500
    #     MOEA_D(data, config, 'basic', None).solve(opt_step=50, con_file=file)
