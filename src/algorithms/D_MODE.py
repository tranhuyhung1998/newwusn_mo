from itertools import combinations
import random
import numpy as np

from src.commons.input import WusnInput
from src.algorithms.individual.de_gene import DEGene
from src.algorithms.module.mo_selection import select, fast_non_dominated_sort


def pareto_based_reproduction(P: list, p_g, p_m, p_p):
    # reproduction
    Q = []
    p_bests = fast_non_dominated_sort(P)[0]

    for p in P:
        p_best = p_bests[random.randint(0, len(p_bests)-1)]
        Q.append(p.dmode_reproduction(p_best, P, p_g, p_m, p_p))

    # selection
    R = P + Q
    P = select(R, len(P))
    return P


def D_MODE(data: WusnInput, max_generations=250, population_size=None,
           p_g=0.3, p_m=0.1, p_p=0.2):
    assert p_g + p_m + p_p < 1, 'wrong param!'

    # init population
    N = population_size
    if N is None:
        N = data.max_population(data.num_of_sensors)
    else:
        N = data.max_population(N)
    P = []
    for _ in range(N):
        P.append(DEGene(data))

    # differential evolution
    for k in range(max_generations):
        P = pareto_based_reproduction(P, p_g, p_m, p_p)

    # return final Pareto
    P = fast_non_dominated_sort(P)
    P[0].sort(key=lambda p: p.f[0])
    return P[0]


if __name__ == '__main__':
    d = WusnInput.from_file('../../data/small/uu-dem3_r50_1.json')
    F = D_MODE(d, 250, 40)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))
