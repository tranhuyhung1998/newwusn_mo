import math
import heapq
import numpy as np
import random

from src.algorithms.individual.ga_gene import GAGene
from src.commons.input import WusnInput
# from commons.output import pareto_con_file
# from commons.optimizer import optimize


def distance(p1: GAGene, p2: GAGene):
    return np.linalg.norm(p1.norm() - p2.norm())


def fitness_assignment(P: list):
    N = len(P)

    # calculate raw fitness R
    dominators = np.empty(N, dtype=object)
    S = np.zeros(N, dtype=int)
    R = np.empty(N, dtype=float)
    for i, p in enumerate(P):
        dominators[i] = []
        for j, q in enumerate(P):
            if i != j and q.dominate(p):
                dominators[i].append(j)
                S[j] += 1
    for i in range(N):
        R[i] = sum(S[j] for j in dominators[i])

    # Calculate density D
    k = int(math.sqrt(N))
    D = np.empty(N, dtype=float)
    for i, p in enumerate(P):
        distances = [distance(p, q) for q in P if p != q]
        if len(distances) == 0:
            print(P)
        assert k <= len(distances)
        sigma = heapq.nsmallest(k, distances)[-1]
        D[i] = 1 / (sigma + 2)

    F = R + D
    for i, p in enumerate(P):
        p.F = F[i]


def environmental_selection(P: list, N: int):
    P1 = [p for p in P if p.F < 1]

    # add
    if len(P1) < N:
        dominated = [p for p in P if p.F >= 1]
        dominated.sort(key=lambda p: p.F)
        P1.extend(dominated[:N - len(P1)])
    # truncate
    else:
        while len(P1) > N:
            N1 = len(P1)
            mat = np.empty([N1, N1], dtype=float)
            for i in range(N1):
                for j in range(N1):
                    mat[i, j] = float('inf') if i == j else distance(P1[i], P1[j])
            np.sort(mat, axis=0)
            candidates = list(range(N1))
            for j in range(N1):
                candidates = [i for i in candidates if mat[i, j] ==
                              min([mat[i, j] for i in candidates])]
                if len(candidates) == 1:
                    break
            del P1[candidates[0]]

    return P1


def SPEA2(data: WusnInput, N=0, N_=0, T=100,
          p_c=0.7, p_m=0.1, opt_mode=None):
    if N == 0:
        N = data.num_of_sensors
    if N_ == 0:
        N_ = data.num_of_sensors

    # Step 1: Initialization
    P = []
    P_ = []
    for _ in range(N):
        P.append(GAGene(data))

    t = 0
    while True:
        P_ = P + P_
        assert len(P_) >= N, 'step 2- wrong with %i' % t

        # Step 2: Fitness assignment
        fitness_assignment(P_)

        # Step 3: Environmental selection
        P_ = environmental_selection(P_, N_)
        assert len(P_) == N_, 'step 3 wrong'

        # Step 4: Termination
        if t >= T:
            return P_

        # Step 5: Mating seletion
        P = GAGene.binary_tournament(P_, N)
        P = list(P)
        assert len(P) == N, 'step 5 wrong'

        # Step 6: Variation
        for i in range(len(P) // 2):
            if random.random() < p_c:
                P[i], P[-i-1] = GAGene.relay_crossover(P[i], P[-i - 1])
        for p in P:
            p.mutation(p_m)

        # P = P1
        # P_ = P1_
        t += 1
        # print(t)


if __name__ == '__main__':
    d = WusnInput.from_file('../data/dataset 1s/ga-dem1_r25_1.in')
    F = SPEA2(d, N=20, N_=20, T=10)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))
