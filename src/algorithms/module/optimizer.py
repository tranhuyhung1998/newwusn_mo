from random import random

from sortedcontainers import SortedList
import numpy as np

from src.algorithms.individual.ga_gene import GAGene


def optimize(g: GAGene, opt_mode=0.5):
    if opt_mode == 'relay':
        relay_clean(g)
        energy_clean(g)
    elif opt_mode == 'energy':
        energy_clean(g)
        relay_clean(g)
    else:
        if random() < opt_mode:
            relay_clean(g)
            energy_clean(g)
        else:
            energy_clean(g)
            relay_clean(g)
    g.update()


def energy_clean(g):
    sn_cnt = np.zeros([g.dat.num_of_relays], dtype=int)
    for rn in g.cms:
        sn_cnt[rn] += 1

    cons = [(i, False) for i in range(g.dat.num_of_sensors)] + \
           [(j, True) for j in range(g.dat.num_of_relays) if sn_cnt[j] > 0]
    connections = SortedList(cons, key=lambda x: val(g, x, sn_cnt))

    while True:
        node_id, node_type = connections[-1]
        if node_type:
            if not relay_reduce(g, node_id, connections, sn_cnt):
                break
        # else:
        #     if not sensor_reduce(g, node_id, connections):
        #         break


def relay_clean(g):
    info = g.get_info()
    critical_loss = info[0][1]
    node_id = info[1][0]
    node_type = info[1][1]

    topo = g.topology()
    rns = list(topo.keys())
    for j in rns:
        if not node_type or j != node_id:
            children = topo[j]
            for j1 in topo:
                if j1 != j and g.dat.er(j1, len(topo[j1]) + len(children)) <= critical_loss \
                        and all([g.dat.et(i, j1) != 0 for i in children]) \
                        and all([g.dat.et(i, j1) <= critical_loss for i in children]):
                    topo[j1].extend(children)
                    del topo[j]
                    for i in children:
                        g.cms[i] = j1
                    break


def relay_reduce(g, critical_rn, connections, sn_cnt):
    critical_loss = g.dat.er(critical_rn, sn_cnt[critical_rn])
    # print('  ~ ' + str(critical_loss))
    child_sns = []
    for i in range(g.dat.num_of_sensors):
        if g.cms[i] == critical_rn:
            child_sns.append(i)

    # Move one sensor out of worst relay
    for j in range(g.dat.num_of_relays):
        if j != critical_rn and sn_cnt[j] > 0:
            for i in child_sns:
                if g.dat.et(i, j) != 0 and max(g.dat.et(i, j), g.dat.er(j, sn_cnt[j] + 1)) < critical_loss:
                    connections.remove((critical_rn, True))
                    connections.remove((j, True))
                    connections.remove((i, False))

                    g.cms[i] = j
                    sn_cnt[critical_rn] -= 1
                    sn_cnt[j] += 1

                    if sn_cnt[critical_rn] > 0:
                        connections.add((critical_rn, True))
                    connections.add((j, True))
                    connections.add((i, False))

                    return True

    # Replace worst relay
    for j in range(g.dat.num_of_relays):
        if sn_cnt[j] == 0:
            if g.dat.er(j, sn_cnt[critical_rn]) < critical_loss:
                good = True
                for i in child_sns:
                    if g.dat.et(i, j) == 0 or g.dat.et(i, j) >= critical_loss:
                        good = False
                        break
                if good:
                    connections.remove((critical_rn, True))

                    for i in child_sns:
                        connections.remove((i, False))
                        g.cms[i] = j
                        connections.add((i, False))

                    sn_cnt[j] = sn_cnt[critical_rn]
                    sn_cnt[critical_rn] = 0
                    connections.add((j, True))

                    return True

    return False


def sensor_reduce(g, critical_sn, connections):
    critical_rn = g.cms[critical_sn]
    critical_loss = g.dat.et(critical_sn, critical_rn)

    for i in range(g.dat.num_of_sensors):
        j = g.cms[i]

        d1 = g.dat.et(i, critical_rn)
        d2 = g.dat.et(critical_sn, j)
        if d1 != 0 and d2 != 0 and max(d1, d2) < critical_loss:
            connections.remove((critical_sn, False))
            connections.remove((i, False))

            g.cms[i] = critical_rn
            g.cms[critical_sn] = j

            connections.add((critical_sn, False))
            connections.add((i, False))

            return True

    return False


def val(g, x, sn_cnt):
    node, node_type = x
    if node_type:
        return g.dat.er(node, sn_cnt[node])
    else:
        return g.dat.et(node, g.cms[node])
