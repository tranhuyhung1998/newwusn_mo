
def select(R, n):
    F = fast_non_dominated_sort(R)
    P = []
    i = 0

    while len(F[i]) > 0 and len(P) + len(F[i]) <= n:
        P += F[i]
        i += 1

    if n != len(P):
        I = crowding_distance_assignment(F[i])
        F[i].sort(key=lambda p: I[p], reverse=True)

        P += F[i][0: (n - len(P))]

    return P


def fast_non_dominated_sort(P: list):
    F = [[]]

    i = 0

    for p in P:
        p.S = []
        p.n = 0
        p.stt = i
        i += 1

        for q in P:
            if p.dominate(q):
                p.S.append(q)
            elif q.dominate(p):
                p.n += 1
        if p.n == 0:
            p.rank = 0
            F[0].append(p)

    # for p in P:
    #     print("Dominated: " + str(p.n) + "  Dominating: " + str([o.stt for o in p.S]))

    i = 0
    while len(F[i]) > 0:
        Q = []
        for p in F[i]:
            for q in p.S:
                q.n -= 1
                if q.n == 0:
                    q.rank = i + 1
                    Q.append(q)
        i += 1
        F.append(Q)

    # for p in P:
    #     print(p.n)

    return F


def crowding_distance_assignment(P: list):
    I = {}
    n = len(P)
    for i in range(n):
        I[P[i]] = 0

    for m in range(len(P[0].f)):
        P.sort(key=lambda p: p.f[m])
        I[P[0]] = I[P[-1]] = float('inf')
        delta_f = P[-1].f[m] - P[0].f[m]
        if delta_f != 0.0:
            for i in range(1, n-1):
                I[P[i]] += (P[i+1].f[m] - P[i-1].f[m]) / delta_f
    return I
