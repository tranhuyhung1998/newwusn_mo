import numpy as np
import random

from src.commons.input import WusnInput
from src.algorithms.individual.gene import Gene


class DEGene(Gene):
    def __hash__(self):
        return id(self)

    def clone(self):
        p = DEGene(self.dat, False)
        p.cms = np.copy(self.cms)
        p.f = self.f
        return p

    def dmode_reproduction(self, p_best, P: list, p_g, p_m, p_p):
        p = self.clone()

        for i in range(self.dat.num_of_sensors):
            assert len(self.dat.links[i]) > 0, 'wtf'
            if len(self.dat.links[i]) == 1:
                continue

            r = random.random()
            if p_g < r:
                p.cms[i] = p_best.cms[i]
            elif p_g + p_m < r:
                p.cms[i] = self.random_link(i, p.cms[i])
            elif p_g + p_m + p_p < r:
                a_j = i
                while a_j == i:
                    a_j = random.randint(0, len(self.dat.links[i]))
                p.cms[i] = P[a_j].cms[i]

        p.update()
        return p
