import numpy as np
from collections import defaultdict

from src.commons.input import WusnInput


class Gene:
    def __init__(self, dat: WusnInput, rand=True):
        self.dat = dat
        self.cms = np.empty(shape=[dat.num_of_sensors], dtype=int)
        self.f = None
        if rand:
            for i in range(dat.num_of_sensors):
                self.cms[i] = self.random_link(i, -1)
            self.update()

    def __eq__(self, other):
        return np.array_equal(self.cms, other.cms)

    def __ne__(self, other):
        return not self.__eq__(other)

    def validate(self):
        data = self.dat
        for i in range(self.dat.num_of_sensors):
            if not data.communicable(data.sensors[i], data.relays[self.cms[i]]):
                return False
        return True

    def count_relay_load(self):
        x = np.zeros([self.dat.num_of_relays], dtype=int)
        for i in range(self.dat.num_of_sensors):
            x[self.cms[i]] += 1
        return x

    def objective_function(self, alpha: float):
        return alpha * self.f[0] / self.dat.num_of_relays + (1 - alpha) * self.f[1] / self.dat.e_max

    def get_info(self):
        ec = 0.0
        node_id = -1
        node_type = False

        for i in range(self.dat.num_of_sensors):
            e = self.dat.et(i, self.cms[i])
            if ec < e:
                ec = e
                node_id = i
                node_type = False

        x = self.count_relay_load()
        for j in range(self.dat.num_of_relays):
            if x[j] > 0:
                e = self.dat.er(j, x[j])
                if ec < e:
                    ec = e
                    node_id = j
                    node_type = True

        return [[np.count_nonzero(x), ec], [node_id, node_type]]

    def get_objectives(self):
        return self.get_info()[0]

    def get_critical_node(self):
        return self.get_info()[1]

    def topology(self):
        children = defaultdict(list)
        for i in range(self.dat.num_of_sensors):
            children[self.cms[i]].append(i)
        return children

    def update(self):
        self.f = self.get_objectives()

    def norm(self):
        return self.f / self.dat.maxs

    def random_link(self, i, ban):
        lim = len(self.dat.links[i])
        new_id = np.random.randint(lim)
        new_cms = self.dat.links[i][new_id]
        if new_cms != ban or lim == 1:
            return new_cms
        new_cms = np.random.randint(lim-1)
        if new_cms >= new_id:
            new_cms += 1
        return self.dat.links[i][new_cms]

    # def print(self):
    #     print(self.cms)

    def dominate(self, p):
        dom_exist = False

        for i in range(len(self.f)):
            if self.f[i] > p.f[i]:
                return False
            if self.f[i] < p.f[i]:
                dom_exist = True

        return dom_exist
