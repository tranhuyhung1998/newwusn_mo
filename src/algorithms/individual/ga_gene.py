import numpy as np

from src.commons.input import WusnInput
from src.algorithms.individual.gene import Gene


class GAGene(Gene):
    def __hash__(self):
        return id(self)

    def clone(self):
        p = GAGene(self.dat, False)
        p.cms = np.copy(self.cms)
        p.f = self.f
        return p

    @staticmethod
    def binary_tournament(P: list, N: int):
        for _ in range(N):
            p1 = P[np.random.randint(len(P))].clone()
            p2 = P[np.random.randint(len(P))].clone()
            if p1.dominate(p2):
                yield p1
            elif p2.dominate(p1):
                yield p2
            else:
                yield p1 if np.random.randint(2) == 0 else p2

    @staticmethod
    def one_point_crossover(p1, p2):
        cut = np.random.randint(1, p1.dat.num_of_sensors)

        y1 = GAGene(p1.dat, False)
        y1.cms = np.concatenate((p1.cms[:cut], p2.cms[cut:]))
        y1.update()

        y2 = GAGene(p2.dat, False)
        y2.cms = np.concatenate((p2.cms[:cut], p1.cms[cut:]))
        y2.update()

        return y1, y2

    @staticmethod
    def relay_crossover(p1, p2):
        topo1 = p1.topology()
        topo2 = p2.topology()

        y1 = p1.clone()
        for j in topo2:
            for i in topo2[j]:
                y1.cms[i] = j
        y1.update()

        y2 = p2.clone()
        for j in topo1:
            for i in topo1[j]:
                y2.cms[i] = j
        y2.update()

        return y1, y2

    def mutation(self, p_m: float):
        for i in range(self.dat.num_of_sensors):
            if np.random.random() < p_m:
                self.cms[i] = self.random_link(i, self.cms[i])
        self.update()

    def relay_mutation(self, p_m: float):
        pass


if __name__ == '__main__':
    inp = WusnInput.from_file('../data/dataset 2s/no-dem1_r25_1.in')
    encoded = '36 35 19 13 35 24 2 8 26 16 24 6 6 37 9 5 25 3 36 5 7 7 15 32 2 26 19 8 29 33 20 0 0 25 9 17 29 15 1 12'
    gen = GAGene(inp, rand=False)
    gen.cms = [int(x) for x in encoded.split()]
    gen.update()
    print(gen.f)
