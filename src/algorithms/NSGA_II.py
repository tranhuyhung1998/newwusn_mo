import numpy as np

from src.algorithms.individual.ga_gene import GAGene
from src.algorithms.module.mo_selection import select, fast_non_dominated_sort
from src.commons.input import WusnInput
from src.commons.output import pareto_con_file
from src.algorithms.module.optimizer import optimize


# def order(p1: Gene, p2: Gene):
#     return p1.rank < p2.rank or (p1.rank == p2.rank and p1.distance > p2.distance)


def NSGAII(data: WusnInput, max_generations=250, population_size=None,
           p_c=0.7, p_m=0.1, opt_mode=None, con_file=None, con_step=1):
    n = population_size
    if n is None:
        n = data.max_population(data.num_of_sensors)
    else:
        n = data.max_population(n)

    P = []
    while len(P) < n:
        g = GAGene(data)
        if not P.__contains__(g):
            P.append(g)

    if con_file is not None:
        pareto_con_file(fast_non_dominated_sort(P)[0], con_file, 0)

    for k in range(max_generations):
        Q = make_new_pop(P, n, p_c, p_m)
        R = list(set(P + Q))
        P = select(R, n)
        if opt_mode is not None:
            for p in P:
                optimize(p)

        if con_file is not None and (k + 1) % con_step == 0:
            pareto_con_file(fast_non_dominated_sort(P)[0], con_file, k+1)

    P = fast_non_dominated_sort(P)
    P[0].sort(key=lambda p: p.f[0])

    # if opt_mode is not None:
    #     for p in P[0]:
    #         optimize(p)
    return P[0]


def make_new_pop(P: list, n: int, p_c: float, p_m: float):
    Q = GAGene.binary_tournament(P, n)
    Q = list(Q)

    for i in range(0, n >> 1):
        if np.random.random() < p_c:
            Q[i], Q[n - i - 1] = GAGene.relay_crossover(Q[i], Q[n - i - 1])

    for p in Q:
        p.mutation(p_m)

    return Q


# def get_best(P: list):
#     f = float('inf')
#     for p in P:
#         f = min(f, p.objective_function(0.5))
#     return f


if __name__ == '__main__':
    d = WusnInput.from_file('../data/medium/uu-dem3_r50_1.in')
    F = NSGAII(d, 250)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))

    print('---')

    F = NSGAII(d, 500)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))

    print('---')

    F = NSGAII(d, 1000)
    for p in F:
        print(str(p.f[0]) + ' ' + str(p.f[1]))
