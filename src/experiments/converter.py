import os
import pandas as pd


def grouping(df: pd.DataFrame):
    def get_dataset(test: str):
        datasets = {
            'ga-r25': 'dataset 1',
            'no-r25': 'dataset 2',
            'uu-r25': 'dataset 3',
            'uu-r30': 'dataset 4',
            'uu-r45': 'dataset 5'
        }
        parts = test.split('_')
        return datasets[parts[0][:3] + parts[1]]

    return df.groupby(get_dataset).mean()


def compare_by_base(df: pd.DataFrame, base: str):
    columns = [col for col in df.columns if col != base]
    for col in columns:
        df[col] = df.apply(lambda x: 1 if x[base] > x[col] else 0, axis=1)
    del df[base]
    return df


if __name__ == '__main__':
    tab_dir = '../../output/tables'
    stats_dir = '../../output/stats'

    for subdir in os.listdir(tab_dir):
        sheets = dict()
        for tab in os.listdir(os.path.join(tab_dir, subdir)):
            df = pd.read_csv(os.path.join(tab_dir, subdir, tab), index_col='Test')
            if 'hypervolume' in tab:
                df = compare_by_base(df, 'MOEA/D-LS')
            df = grouping(df)
            df.loc['Average'] = df.mean()

            sp = tab.rfind('_')
            xlsx_path = os.path.join(stats_dir, subdir, tab[:sp] + '.xlsx')
            if xlsx_path not in sheets:
                sheets[xlsx_path] = dict()
            sheets[xlsx_path][tab[sp+1:-4]] = df

        os.makedirs(os.path.join(stats_dir, subdir), exist_ok=True)
        for sheet in sheets:
            writer = pd.ExcelWriter(sheet)
            for dtype in sheets[sheet]:
                sheets[sheet][dtype].to_excel(writer, dtype, index_label='Data')
            writer.save()
