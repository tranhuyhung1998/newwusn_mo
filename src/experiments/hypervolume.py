import os

from src.utils.metrics import read_pareto, hypervolume_2_objs


if __name__=='__main__':
    out_dir = '../output/text/moea_d_opt_mod2_1'
    tab_dir = '../../output/tables'

    for dtype in sorted(os.listdir(out_dir)):
        with open(os.path.join(tab_dir, 'hypervolume_' + dtype + '.csv'), 'w') as f:
            for file in sorted(os.listdir(os.path.join(out_dir, dtype))):
                if not file.endswith('.out'):
                    continue
                if 'r50' in file or ('uu' not in file and 'r25' not in file):
                    continue
                print(os.path.join(dtype, file))
                v = read_pareto(os.path.join(out_dir, dtype, file))
                hyp = hypervolume_2_objs(v, maximize=False)
                f.write(file + ',' + str(hyp) + '\n')


