import os

from src.commons.input import WusnInput

if __name__ == '__main__':
    data_dir = '../../data/'
    for dtype in os.listdir(data_dir):
        for file in os.listdir(os.path.join(data_dir, dtype)):
            if not file.endswith('in'):
                continue
            file_path = os.path.join(data_dir, dtype, file)
            data = WusnInput.from_file(file_path)
            with open(file_path.replace('in', 'norm'), 'w+') as f:
                f.write(str(data.num_of_relays) + ' ' + str(data.e_max) + '\n')
