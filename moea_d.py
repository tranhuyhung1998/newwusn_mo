import os
import time
from argparse import ArgumentParser
from joblib import Parallel, delayed

from src.commons.input import WusnInput
from src.commons.output import pop_to_pareto, pareto_to_file, detail_to_file
from src.algorithms.MOEA_D import MOEA_D, WusnConfig


def parse_arguments():
    parser = ArgumentParser()

    parser.add_argument('-i', '--input', default='data')
    parser.add_argument('-o', '--output', default='output/text/moead')
    parser.add_argument('-c', '--count', default='30', help='number of run times')
    parser.add_argument('-ops', '--opt-step', default='0', help='optimizer insertion distance')
    parser.add_argument('-opm', '--opt-mode', default='mix', help='relay | energy | mix')
    parser.add_argument('-n', '--sub-problems', default='41')
    parser.add_argument('-t', '--neighborhood', default='3')
    parser.add_argument('-ri', '--ref-init', default=None, help='init reference point or not')
    parser.add_argument('-m', '--mode', default='write', help='write | append | converge | cms | detail')
    parser.add_argument('-cs', '--con-step', default='2', help='print convergence step')
    parser.add_argument('-g', '--gen', default='10', help='max generations')
    parser.add_argument('-sg', '--stall-gen', default='10', help='max stall generations')
    parser.add_argument('-p', '--parallel', default='a', help='run parallel or consecutive')

    return parser.parse_args()


def solve(dtype, file, mode):
    save_path = os.path.join(out_path, dtype, file.split('.')[0] + '.out')
    if mode == 'append' and os.path.exists(save_path):
        return

    print(os.path.join(dtype, file))
    data = WusnInput.from_file(os.path.join(inp_dir, file))

    if mode == 'converge' or mode == 'cms':
        with open(save_path.replace('.out', '.con'), 'w') as f:
            MOEA_D(data, config, args.ref_init).solve(int(args.opt_step), args.opt_mode, f, int(args.con_step))
    elif mode == 'detail':
        if os.path.exists(save_path):
            os.remove(save_path)
        for _ in range(int(args.count)):
            t = time.process_time()
            res = pop_to_pareto(MOEA_D(data, config, args.ref_init).solve(int(args.opt_step), args.opt_mode))
            t = time.process_time() - t
            detail_to_file(res, t, save_path)
    else:
        raw = []
        for _ in range(int(args.count)):
            last_pop = MOEA_D(data, config, args.ref_init).solve(int(args.opt_step), args.opt_mode)
            raw.extend(last_pop)

        res = pop_to_pareto(raw)
        for inv in res.values():
            assert inv.validate(), 'INVALID SOLUTION!!!'
        pareto_to_file(res, save_path, save_path.replace('.out', '.inv'))


def script_filter(filename):
    return 'r50' not in filename and ('r25' in filename or 'uu' in filename)


if __name__ == '__main__':
    args = parse_arguments()

    config = WusnConfig.from_file('config.json', int(args.sub_problems),
                                  int(args.neighborhood), int(args.gen), int(args.stall_gen))

    out_path = args.output

    # out_path += '_' + args.norm
    if args.opt_step != '0':
        out_path += '_opt-' + args.opt_mode
    if args.mode == 'detail' or args.mode == 'converge':
        out_path += '_' + args.mode

    print('--- Running ' + out_path + ' ---')
    for dtype in sorted(os.listdir(args.input)):
        inp_dir = os.path.join(args.input, dtype)
        os.makedirs(os.path.join(out_path, dtype), exist_ok=True)
        test_list = [file for file in sorted(os.listdir(inp_dir)) if script_filter(file)
                     and (args.mode != 'append' or
                          not os.path.exists(os.path.join(out_path, dtype, file.split('.')[0] + '.out')))]
        if args.parallel is not None:
            Parallel(n_jobs=-1)(delayed(solve)(dtype, file, args.mode) for file in test_list)
        else:
            for file in test_list:
                solve(dtype, file, args.mode)
