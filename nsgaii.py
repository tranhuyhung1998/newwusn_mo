import time
from os.path import join
import os
from argparse import ArgumentParser
from joblib import Parallel, delayed

from src.commons.input import WusnInput
from src.algorithms.NSGA_II import NSGAII
from src.commons.output import pop_to_pareto, pareto_to_file, detail_to_file


def parse_arguments():
    parser = ArgumentParser()

    parser.add_argument('-i', '--input', default='data')
    parser.add_argument('-o', '--output', default='output/text/nsgaii')
    parser.add_argument('-c', '--count', default='30', help='number of run times')
    parser.add_argument('-n', '--population-size', default='40')
    parser.add_argument('-m', '--mode', default='write', help='write | append | converge | detail')
    parser.add_argument('-cs', '--con-step', default='2', help='print convergence step')
    parser.add_argument('-op', '--optimizer', default=None, help='optimize results or not')
    parser.add_argument('-g', '--generations', default='10', help='number of generations')

    return parser.parse_args()


def solve(file, inp, out):
    run_count = int(args.count)
    print(join(dtype, file))
    d = WusnInput.from_file(join(inp, file))
    save_path = os.path.join(out, file.split('.')[0] + '.out')

    if args.mode == 'converge':
        with open(save_path.replace('.out', '.con'), 'w') as con_file:
            NSGAII(d, max_generations=int(args.generations), population_size=int(args.population_size),
                   opt_mode=args.optimizer, con_file=con_file, con_step=int(args.con_step))
    elif args.mode == 'detail':
        if os.path.exists(save_path):
            os.remove(save_path)
        for _ in range(int(args.count)):
            t = time.process_time()
            res = pop_to_pareto(NSGAII(d, opt_mode=args.optimizer))
            t = time.process_time() - t
            detail_to_file(res, t, save_path)
    else:
        R = []
        for _ in range(run_count):
            R.extend(NSGAII(d, opt_mode=args.optimizer))

        res = pop_to_pareto(R)
        pareto_to_file(res, save_path)


def script_filter(filename):
    return 'r50' not in filename and ('r25' in filename or 'uu' in filename)


def parallel_list(inp, mode):
    return [file for file in sorted(os.listdir(inp))
            if script_filter(file) and (mode != 'append' or
                                        not os.path.exists(os.path.join(inp, file)))]


if __name__ == '__main__':
    args = parse_arguments()
    # os.makedirs(args.output, exist_ok=True)
    out_folder = args.output
    if args.optimizer is not None:
        out_folder += '_opt'
    if args.mode == 'detail':
        out_folder += '_detail'

    print('--- Running ' + out_folder + ' ---')
    for dtype in sorted(os.listdir(args.input)):
        inp_dir = join(args.input, dtype)
        out_dir = os.path.join(out_folder, dtype)
        os.makedirs(os.path.join(out_folder, dtype), exist_ok=True)
        Parallel(n_jobs=-1)(delayed(solve)(file, inp_dir, out_dir)
                            for file in parallel_list(inp_dir, args.mode))
